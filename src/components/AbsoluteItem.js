import React from 'react';

export const AbsoluteItem = ({children, positions}) => {
  return (
    <div className='position-absolute'
         style={{top: positions.top, left: positions.left, bottom: positions.bottom, right: positions.right}}>
      {children}
    </div>
  )
};