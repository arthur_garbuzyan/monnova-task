import React from 'react';

export const Flex = ({children, className}) => {
    return(
        <div className={`d-flex ${className}`}>
            {children}
        </div>
    )
};