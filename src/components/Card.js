import React from 'react';
import {Flex} from "./Flex";

export const Card = ({children}) => {
    return(
        <Flex className='card d-flex-column'>
            {children}
        </Flex>
    )
};