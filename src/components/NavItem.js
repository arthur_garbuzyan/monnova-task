import React from 'react';

export const NavItem = ({children, isActive, onClick}) => {

    return(
       <div onClick={onClick} className={`${isActive ? 'nav-item-active' : 'nav-item'}`}>
           {children}
       </div>
    )
};