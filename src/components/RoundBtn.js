import React from 'react';

export const RoundBtn = ({children, isActive, onClick}) => {

    return(
        <div className={`${isActive ? 'round-btn-active' : 'round-btn'}`} onClick={onClick}>
            {children}
        </div>
    )
};