import React from 'react';
import {Card} from "./Card";
import {Flex} from "./Flex";

export const ContestItem = ({children}) => {
    return(
        <Flex className='mt-25 contestItem'>
            <Card>{children}</Card>
        </Flex>
    )
};