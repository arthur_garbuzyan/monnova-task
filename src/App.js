import React, {useEffect, useState} from 'react';
import './App.css';
import {Card} from "./components/Card";
import {NavItem} from "./components/NavItem";
import {RoundBtn} from "./components/RoundBtn";
import {Flex} from "./components/Flex";
import {ContestItem} from "./components/ContestItem";
import {AbsoluteItem} from "./components/AbsoluteItem";
import {Button} from "./components/Button";

function App() {

  const [metrics, setMetrics] = useState({
    1: {
      icon: 'A',
      id: 1,
      isActive: true
    },
    2: {
      icon: 'B',
      id: 2,
      isActive: false
    },
    3: {
      icon: 'C',
      id: 3,
      isActive: false
    },
    5: {
      icon: 'D',
      id: 5,
      isActive: false
    },
    9: {
      icon: 'E',
      id: 9,
      isActive: false
    },
    16: {
      icon: 'F',
      id: 16,
      isActive: false
    },
  });
  const [error, setError] = useState(null);
  const [activeNav, setActiveNav] = useState('inprocess');
  const [activeMetrics, setActiveMetrics] = useState('');
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  useEffect( () => {
    setIsLoaded(false);
     fetch(`https://frontapi.inkin.com/getContests.php?state=${activeNav}&${activeMetrics && 'metrics='+activeMetrics}`, )
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        (error) => {
          setIsLoaded(true);
          alert(error);
          setError(error);
        }
      )
  },[activeNav, activeMetrics]);

  useEffect(() => {

    let activeMetrics = [];

    Object.keys(metrics).map((val, key) => {
      if (metrics[val].isActive) {
        activeMetrics.push(metrics[val].id)
      }
    });

    setActiveMetrics(activeMetrics.join(','))

  }, [metrics]);

  useEffect(() => {

  }, [activeNav]);

  const setMetricsActive = (key) => {

    let newMetrics = {...metrics};
    newMetrics[key].isActive = !metrics[key].isActive;
    setMetrics(newMetrics)
  };

  return (
    <Flex className="container d-flex-column">
      <Card>
        <Flex className='border-bottom'>
          <NavItem isActive={activeNav === 'inprocess'} onClick={() => setActiveNav('inprocess')}><span>Ongoing</span></NavItem>
          <NavItem isActive={activeNav === 'upcoming'} onClick={() => setActiveNav('upcoming')}>Upcoming</NavItem>
          <NavItem isActive={activeNav === 'past'} onClick={() => setActiveNav('past')}>Ended</NavItem>
        </Flex>
        <Flex className='d-flex-column p-15'>
          <Flex className="metrics-title">metrics</Flex>
          <Flex className='filter-items'>
            {Object.keys(metrics).map((val, key) => {
              return (
                <RoundBtn key={key} isActive={metrics[val].isActive} onClick={() => setMetricsActive(val)}>
                  {metrics[val].icon}
                </RoundBtn>
              )
            })}
          </Flex>
        </Flex>
      </Card>
      {!isLoaded ? (
        <Flex className='flex-full-center mt-25'>
          <h2>Loading...</h2>
        </Flex>
      ) : (
        <Flex className='d-flex-wrap d-flex-space-between'>
          {items.map((val, key) => {
            return <ContestItem key={key}>
              <Flex className='contestItem-first-section flex-full-center position-relative'>
                <span>{metrics[val.metric].icon}</span>
                <AbsoluteItem positions={{bottom: "15px", left: "10px"}}>
                  <Button className='text-bold'>
                    {val.type}
                  </Button>
                </AbsoluteItem>
                <AbsoluteItem positions={{bottom: "10px", right: "4px"}}>
                  <RoundBtn>
                    <div className='icons-color'>{metrics[val.metric].icon}</div>
                  </RoundBtn>
                </AbsoluteItem>
              </Flex>
              <Flex className='contestItem-second-section p-15'>
                <Flex className='d-flex-column full-width'>
                  <h2 className='text-bold'>{val.title}</h2>
                  <Flex className='m-top-35 text-bold'>{val.date.start} - {val.date.end}</Flex>
                  <Flex className='flex-full-center full-height'>
                    <Button className='join-contest'>
                      <span className='text-bold'>JOIN CONTEST</span>
                    </Button>
                  </Flex>
                </Flex>
              </Flex>
            </ContestItem>
          })}
        </Flex>
      )}
    </Flex>
  );
}

export default App;
